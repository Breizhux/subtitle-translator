![icône subtitle-translator](https://gitlab.com/Breizhux/subtitle-translator/-/raw/main/icons/96x96_subtitle-translator.png) Subtitle-Translator
===============

Un outil en ligne de commande pour créer les sous-titres de vos vidéos, et les traduire immédiatement en une autre langue.
Ainsi, vous pourrez créer les sous-titres français pour une vidéo où les personnes parlent en chinois, en russe ou simplement en anglais, et vous pourrez comprendre ce qui s'y dit !

**Basé sur whisper**
Le moteur d'IA utilisé pour générer les sous-titre est whisper, via la bibliothèque [faster-whisper](https://github.com/SYSTRAN/faster-whisper).
Le moteur speech-to-text fonctionne donc en local.

**Traducteur**
Le moteur de traduction par défaut est celui que j'ai développé à côté : [simple-translator](https://gitlab.com/Breizhux/simple-translator)
Ce moteur est simple, mais assez lent.

Cependant, vous pouvez très bien utiliser le code en renseignant votre propre moteur de traduction (uniquement en mode python directement, pas en mode cli). Pour cela, se référer à la section *Description* et *Utilisation*.


**Attention !** Seuls les fichiers de sous-titre `.srt` sont pris en charge.


## Installation

Simplement, avec pip :
```
pip install git+https://gitlab.com/Breizhux/subtitle-translator.git
```


## Utilisation

### En mode cli directement

```bash
subtitle-translator "path/to/file.mp4" "fr" -sst "medium"
#ou
subtitle-translator "path/to/file.srt" "fr"
```
1. Donner les chemins vers votre vidéo, ou vers le fichier de sous-titre dont vous disposez déjà.
2. Donner la langue cible.
3. Si en `1.` vous avez donnez une vidéo, vous devez dire quel modèle speech-to-text doit être utilisé (par défaut: medium).


### Dans votre code python

```python
import subtitle_translator

#générer le fichier de sous-titre
subtitle_translator.speech_to_text("path/to/file.mp4", model='medium')

#traduire un fichier de sous-titre simplement :
subtitle_translator.translate_srt("path/to/file.srt", "fr")             #non fonctionnel, car pas encore de moteur de traduction par défaut

#maîtriser la traduction du fichier de sous-titre :
sentences = Sentences()                         #crée une sorte de list, qui correspondent aux phrases des sous-titres.
sentences.load_from_file("path/to/file.srt")    #charge et découpe les phrases depuis un fichier.
sentences.set_translation_engine(your_translator_engine) #configure votre moteur de traduction préféré (cf description)
sentences.translate("fr")                       #traduit chaque phrase dans la langue souhaitée
sentences.generate_subtitle()                   #génère les sous-titres dans la nouvel langue
sentences.write_subtitle(f"{os.path.splitext(filename)[0]}-{language}.srt") #écrit les sous-titres dans le fichier cible.
```


## Description

### Moteur de traduction

Comme vu dans l'usage en code python détaillé, vous pouvez configurer votre propre moteur de traduction.
Cependant, celui-ci doit suivre une certaine forme pour pouvoir fonctionner.
C'est tout simple : `your_func(source_text, target_language, source:optionnal)`


### Génération de sous-titre

Cela paraît simple au premier abord : pour chaque partie de sous titre, on traduit le bout de texte. Mais en fait, pas du tout !
En effet, pour que la phrase soit traduite correctement, il faut traduire toute la phrase d'un coup.

Le fonctionnement de l'algorithme est très bête, pour le moment.
Une `Sentence` correspond à l'ensemble des phrases prononcés lorsque qu'une phrase est commencé au début d'un sous-titre, et qu'une fin de phrase tombe à la fin d'un sous-titre.
Voici un exemple :
```
0
0:0:0,00 --> 0:0:5,00
 Ceci est le début. Mais ici
1
0:0:5,00 --> 0:0:10,00
 la phrase s'arrête. Ce n'est pas pour
2
0:0:10,00 --> 0:0:15,00
 autant qu'elle marque la fin de la section.
```
Dans cet exemple, Sentence prend tout le texte d'un coup et ne découpe pas phrase par phrase. Il garde également le moment de début `0:0:0,00` et celui de fin `0:0:15,00`.
Ainsi, le texte correspondant à Sentence et qui va être traduit d'un bloque est : "*Ceci est le début. Mais ici la phrase s'arrête. Ce n'est pas pour autant qu'elle marque la fin de la section.*".

Ainsi, ce qui est traduit, c'est les phrases qui se suivent les unes derrières les autres.

Puis, pour générer les nouveaux sous-titres, la méthode est un peu brutale, vous conviendrez, mais assez efficace pour la majorité des cas :
1. On regarde le moment où la phrase commence, et là où elle fini. Ici, de `0:0:0,00` à `0:0:15,00`.
2. Ensuite, on découpe la section de texte traduit en autant de fois que l'était la section originel. Ici : `3 fois`.
3. Enfin, on divise le temps total disponible (trouvé en **1.**) en part égale en fonction du nombre trouvé en **2.**.

Les sous-titres approximativement bien dans la majorité des cas (bande annonce, film, conférence).

En revanche, il peut y avoir des décalages lorsque le locuteur ne sait pas trop ce qu'il dit, qu'il coupe ses phrases en plein milieux, ou qu'il ne les termine pas.
Il se peut que l'algorithme évolue pour garder la temporisation des sous-titres originaux, et remplacer seulement les bouts de texte correspondant.

À la base, la réflexion était de dire que s'il y avait un long blanc, c'est que la phrase était terminé. Mais force est de constaté que ça n'est pas toujours le cas...
