#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

from .subtitle_translator import Sentences, Sentence
from .cli import speech_to_text, translate_srt

__all__ = [
    "Sentences",
    "Sentence",
    "speech_to_text",
    "translate_srt",
]
