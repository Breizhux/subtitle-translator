#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import os
from datetime import datetime


DEBUG = False                       #bypass DeepL Scrapper with translated_data.txt
TRANSLATOR = "simple-translator"    #default translator engine

if DEBUG :
    import json
    with open("translated_data.txt", 'r') as file :
        datas = json.load(file)

elif TRANSLATOR == "deepl" :
    import deepl_scrapper
    translator_engine = deepl_scrapper.DeeplScrapper()

elif TRANSLATOR == "simple-translator" :
    import simple_translator
    translator_engine = simple_translator.SimpleTranslator()


def translator(text, target, source='auto') :
    """ Call scrapper for Deepl."""
    if DEBUG :
        for i in datas :
            if i[0] == text : return i[1]
    elif TRANSLATOR == "simple-translator" :
        return translator_engine.translate(text, target, source=source)
    elif TRANSLATOR == "deepl" :
        return translator_engine.translate(text, source=source, target=target)



class Sentences :
    """ List of sentence of a subtitle file."""
    def __init__(self) :
        self.sentences = []

    def __getitem__(self, i) :
        return self.sentences[i]

    def __iter__(self) :
        for i in self.sentences :
            yield i

    def set_translation_engine(self, func) :
        """ Set translation engine for all sentences."""
        for i in self : i.set_translation_engine(func)

    def translate(self, target) :
        """ Translate all sentences."""
        for i in self : i.translate(target)

    def generate_subtitle(self) :
        """ Generate subtitle for all."""
        for i in self : i.generate_subtitle()

    def load_from_file(self, filename) :
        """ Load subtitle from file."""
        end_sentences = [".", "!", "?"]
        with open(filename, 'r') as file :
            srt_file = file.readlines()
        current = None
        for i in range(0, len(srt_file), 4) :
            s = [j.strip('\n') for j in srt_file[i:i+3]]
            if current is None :
                current = Sentence()
                current.add_partial_sentence(s)
            else :
                current.add_partial_sentence(s)
            #si le dernier caractère de la phrase est une fin de phrase...
            if s[2][-1] in end_sentences :
                self.sentences.append(current)
                current = None

    def write_subtitle(self, filename) :
        """ Write generated subtitle in file."""
        with open(filename, 'w') as file :
            i = 0
            for s in self :
                for p in s.get_subtitle_string() :
                    file.write(f"{i}\n")
                    i += 1
                    file.write(p)



class Sentence :
    """ The sentence caracterised by : complete sentence, cut-off number, time start, time end."""
    time_format = "%H:%M:%S,%f"

    def __init__(self) :
        self.translation_engine = translator
        #
        self.text = ""              #Phrases complètes
        self.translation = ""       #Phrases complètes traduites
        self.idx_translation = []   #Phrases complètes traduites et découpée avec les temps correspondants : (text, start, end)
        self.index = []             #index des phrases : (n° d'index, phrase partielle)
        self.start = None           #temps de début de sous-titre
        self.end = None             #temps de fin de sous-titre

    def __len__(self) :
        """ Return number of word in complete source sentence."""
        return self.text

    def __sentence_length(self, sentence) :
        return len(sentence.split(" "))

    def add_partial_sentence(self, srt_lines:list) :
        """ Add a partial sentence subtitle."""
        s, e = srt_lines[1].split(" --> ")
        self.end = self.convert_time_to_datetime(e)
        self.index.append((int(srt_lines[0]), srt_lines[2]))
        if self.text == "" :
            self.text = srt_lines[2].strip(" ")
            self.start = self.convert_time_to_datetime(s)
        else :
            self.text += srt_lines[2]

    def convert_time_to_datetime(self, time:str) :
        """ Convert HH:MM:SS.SSS string to datetime."""
        return datetime.strptime(time, self.time_format)

#    def build_stats(self) :
#        """ Build stats of sentence : of percentage of sentence in first part, in seconds, etc.
#            All saved in self.index : (index n°, partial sentence, percentage)"""
#        new_index = []
#        for i, s in self.index :
#            p = (len(s.strip().split(" "))/len(self)) * 100
#            new_index.append((i, s, p))
#        self.index = new_index

    def set_translation_engine(self, func) :
        """ Set your own translation engine.
        Need this usage : your_func(source_text, target_language, source:optionnal)."""
        self.translate = func

    def translate(self, target, source='auto') :
        """ Call translate engine for own complete sentence"""
        self.translation = self.translation_engine(self.text, target, source=source)

    def generate_subtitle(self) :
        """ Generate subtitle."""
        interval_time = (self.end - self.start) / len(self.index)
        interval_word = round(self.__sentence_length(self.translation) / len(self.index))

        e = self.start
        index_word = 0
        for i in range(len(self.index)) :
            s = e
            e = s + interval_time
            if i+1 == len(self.index) :
                part_sentence = " ".join(self.translation.split(" ")[index_word:])
            else :
                part_sentence = " ".join(self.translation.split(" ")[index_word:index_word+interval_word])
            index_word += interval_word

            self.idx_translation.append((part_sentence, s, e))

    def get_subtitle_string(self) :
        """ Return list of string for subtitle file (.srt)."""
        l_string = []
        for i, p in enumerate(self.idx_translation) :
            string = f"{p[1].strftime('%H:%M:%S,%f')[:-3]} --> {p[2].strftime('%H:%M:%S,%f')[:-3]}\n"
            string += f"{p[0]}\n\n"
            l_string.append(string)
        return l_string



def main(filename, language) :
    sentences = Sentences()
    sentences.load_from_file(filename)
    sentences.translate(language)
    sentences.generate_subtitle()
    sentences.write_subtitle(f"{os.path.splitext(filename)[0]}-{language}.srt")

    if TRANSLATOR == "deepl" : translator_engine.quit()




if __name__ == "__main__" :
    filename = "Séance 2 - Cours 1 (audio).srt"
    language = "en"
    main(filename, language)
