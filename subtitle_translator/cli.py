#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import os
import sys
import argparse
if __package__ == "" : import subtitle_translator
else : from . import subtitle_translator
import faster_whisper_cli


VIDEO_EXTENSIONS = [".mp4", ".avi", ".mkv"]



def speech_to_text(filename, model="medium") :
    """filename: path to audio/video file.
    model: the size of whisper model to use (optionnal, default: medium)."""
    faster_whisper_cli.main(filename, model, tofile=False)
    return f"{os.path.splitext(filename)[0]}.srt"

def translate_srt(filename, language) :
    """ filename: path to .srt subtitle filename.
    language: the target language you want."""
    subtitle_translator.main(filename, language)

def main() :
    if sys.argv[1] in ["-h", "--help"] :
        print("usage: subtitle-translator filepath.mp3 target-language [-stt model size (optionnal)]")
        sys.exit(0)

    parser = argparse.ArgumentParser(description='Translate video subtitle files.')
    parser.add_argument('filename', type=str, help='The path to the video or subtitle file')
    parser.add_argument('language', type=str, help='The target language')
    parser.add_argument('-stt', '--stt-model', type=str, default="medium", help='The name of the whisper model to use')
    args = parser.parse_args()

    print(f"Translate file \"{args.filename}\" to \"{args.language}\" with stt model \"{args.stt_model}\"")
    if os.path.splitext(args.filename)[1] in VIDEO_EXTENSIONS :
        srt_file = speech_to_text(args.filename, args.stt_model)
    else :
        srt_file = args.filename

    print("Running translation.")
    translate_srt(srt_file, args.language)

    print("All are finished !")


if __name__ == "__main__" :
    main()
