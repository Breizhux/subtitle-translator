#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import setuptools

setuptools.setup(
    name = 'subtitle-translator',
    version = '1.0.0',
    description = 'Créer des sous-titres et les traduits dans la langue qui vous chante',
	license = "WTFPL",
    url = 'https://gitlab.com/Breizhux/subtitle-translator',
    author = 'Breizhux',
    author_email = 'xavier.lanne@gmx.fr',
    packages = setuptools.find_packages(
        exclude = [
            'bin',
            'lib',
            '__pycache__',
            'pyvenv.cfg',
            '.gitignore',
            'requirements.txt',
        ],
    ),
    #package_data = {'pkg_name' : [
    #    'path/*',
    #]},
    #include_package_data=True,
    install_requires = [
        'faster-whisper-cli @ git+https://gitlab.com/Breizhux/faster-whisper-cli.git',
        'simple-translator @ git+https://gitlab.com/Breizhux/simple-translator.git',
        #'deepl-scrapper @ git+https://gitlab.com/Breizhux/deepl-scrapper.git',  #optionnal, need creds
    ],
	python_requires = ">=3.8",
    entry_points = {
        'console_scripts': [
            'subtitle-translator = subtitle_translator.cli:main',
        ],
    },
)
